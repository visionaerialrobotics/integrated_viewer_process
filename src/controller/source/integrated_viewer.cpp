/*!*********************************************************************************
 *  \file       integrated_viewer.cpp
 *  \brief      This file implements the IntegratedViewer class
 *  \authors    Abraham Carrera Groba
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/integrated_viewer.h"

#include "ui_integrated_viewer.h"

IntegratedViewer::IntegratedViewer(int argc, char** argv, QWidget* parent) : QMainWindow(parent), ui(new Ui::IntegratedViewer)
{
  ui->setupUi(this);
  setWindowIcon(QIcon(":/img/img/drone-icon.png"));
  onMission = false;
  connectActions();
  // Settings connections
  setUp();
}

IntegratedViewer::~IntegratedViewer()
{
  delete ui;
}

void IntegratedViewer::connectActions()
{
  connect(ui->actionGraphical_User_Interface, SIGNAL(triggered()), this, SLOT(startGraphicalTeleoperation()));
  connect(ui->actionAlphanumeric_Interface, SIGNAL(triggered()), this, SLOT(startAlphanumericInterface()));
  connect(ui->actionPython, SIGNAL(triggered()), this, SLOT(startExecutePythonMission()));
  connect(ui->actionBehavior_tree, SIGNAL(triggered()), this, SLOT(startExecuteBehaviorMission()));

}

void IntegratedViewer::setUp()
{
  n.param<std::string>("robot_namespace", robot_namespace, "drone1");
  n.param<std::string>("window_event_topic", window_event_topic, "window_event");
  n.param<std::string>("mission_state_topic", mission_state_topic, "mission_state");
  n.param<std::string>("my_stack_directory", my_stack_directory, "$(env AEROSTACK_STACK)");
  n.param<std::string>("robot_config_path", configuration_folder, my_stack_directory+"/configs/drone1");
  n.param<std::string>("robot_mission_path", mission_folder, my_stack_directory+"/configs/drone1");

  mission_state_sub =
      n.subscribe("/" + robot_namespace + "/" + mission_state_topic, 10, &IntegratedViewer::missionStateCallback, this);
  window_event_sub =
      n.subscribe("/" + robot_namespace + "/" + window_event_topic, 10, &IntegratedViewer::windowEventCallback, this);
  window_event_pub =
      n.advertise<aerostack_msgs::WindowEvent>("/" + robot_namespace + "/" + window_event_topic, 1, true);
}

void IntegratedViewer::windowEventCallback(const aerostack_msgs::WindowEvent& msg)
{
 if(msg.event==aerostack_msgs::WindowEvent::CLOSE) {

  openedWindows.remove(msg.window);

 }
}

void IntegratedViewer::missionStateCallback(const std_msgs::Bool& msg)
{
  onMission = msg.data;
}

bool IntegratedViewer::isOpened(aerostack_msgs::WindowEvent::_window_type i)
{
  return (std::find(openedWindows.begin(), openedWindows.end(),i) != openedWindows.end());
}




void IntegratedViewer::startExecuteBehaviorMission()
{
  if (!onMission)
  {
          if (!isOpened(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    prefixed_layout:=center \
                    my_stack_directory:=" +
           my_stack_directory +" \
                    robot_config_path:=" +
           configuration_folder + ";exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER;
      window_event_pub.publish(msg);
    }


    if (!isOpened(aerostack_msgs::WindowEvent::EXECUTION_VIEWER))
    {
      system(
          ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::EXECUTION_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::EXECUTION_VIEWER;
      window_event_pub.publish(msg);;

    }

    if (!isOpened(aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics Viewer\"  --command \"bash -c 'roslaunch vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + ";exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER;
      window_event_pub.publish(msg);

    }
    if (!isOpened(aerostack_msgs::WindowEvent::BEHAVIOR_TREE_INTERPRETER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Behavior Tree Interpreter\"  --command \"bash -c 'roslaunch behavior_tree_interpreter behavior_tree_interpreter.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=" +
           my_stack_directory +" \
                    mission_configuration_folder:=" +
           my_stack_directory +"/configs/"+robot_namespace+" \;exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::BEHAVIOR_TREE_INTERPRETER);
   
      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::BEHAVIOR_TREE_INTERPRETER;
      window_event_pub.publish(msg);
    }
  }
  else
  {
    QMessageBox::warning(nullptr, tr("Warning"),
                       tr("Mission in progress. Land  or abort it first"));
  }
}

void IntegratedViewer::startGraphicalTeleoperation()
{
  if (!onMission)
  {
         if (!isOpened(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    prefixed_layout:=center \
                    my_stack_directory:=" +
           my_stack_directory +" \
                    robot_config_path:=" +
           configuration_folder + ";exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER;
      window_event_pub.publish(msg);
    }

    if (!isOpened(aerostack_msgs::WindowEvent::FIRST_PERSON_VIEWER))
    {
      system(
          ("xfce4-terminal  \--tab --title \"First Person Viewer\"  --command \"bash -c 'roslaunch first_view_process first_view.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      system(
          ("xfce4-terminal  \--tab --title \"First Person Viewer\"  --command \"bash -c 'roslaunch first_person_view_process first_person_view.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::FIRST_PERSON_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::FIRST_PERSON_VIEWER;
      window_event_pub.publish(msg);

    }

       if (!isOpened(aerostack_msgs::WindowEvent::EXECUTION_VIEWER))
    {
      system(
          ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::EXECUTION_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::EXECUTION_VIEWER;
      window_event_pub.publish(msg);;

    }
      if (!isOpened(aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics Viewer\"  --command \"bash -c 'roslaunch vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + ";exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER;
      window_event_pub.publish(msg);

    }

    if (!isOpened(aerostack_msgs::WindowEvent::TELEOPERATION_CONTROL))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Teleoperation Control\"  --command \"bash -c 'roslaunch teleoperation_control_window teleoperation_control_window.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::TELEOPERATION_CONTROL);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::TELEOPERATION_CONTROL;
      window_event_pub.publish(msg);
    }
  }
  else
  {
    QMessageBox::warning(nullptr, tr("Warning"),
                       tr("Mission in progress. Land  or abort it first"));
  }
}
void IntegratedViewer::startAlphanumericInterface()
{
  if (!onMission)
  {
       if (!isOpened(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=" +
           my_stack_directory +" \
                    prefixed_layout:=right \
                    robot_config_path:=" +
           configuration_folder + ";exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER;
      window_event_pub.publish(msg);
    }

    if (!isOpened(aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL))
    {
      namespace pt = boost::property_tree;

      std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                            "graphical_user_interface/layouts/"
                                                                            "layout.json");

      pt::read_json(layout_dir, root);

      std::string height = std::to_string(root.get<int>("ALPHANUMERIC_INTERFACE_CONTROL.height"));
      std::string width = std::to_string(root.get<int>("ALPHANUMERIC_INTERFACE_CONTROL.width"));

      std::string x = std::to_string(root.get<int>("ALPHANUMERIC_INTERFACE_CONTROL.position.x"));
      std::string y = std::to_string(root.get<int>("ALPHANUMERIC_INTERFACE_CONTROL.position.y"));
      system(("xfce4-terminal  \ --title \"Alphanumeric Interface Control\"   --geometry " + width + "x" + height +
              "+" + x + "+" + y +
              " --command \"bash -c 'roslaunch alphanumeric_interface alphanumeric_interface.launch --wait \
                    robot_namespace:=" +
              robot_namespace + " \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
                 .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL;
      window_event_pub.publish(msg);

    }
  }
  else
  {
    QMessageBox::warning(nullptr, tr("Warning"),
                       tr("Mission in progress. Land  or abort it first"));
  }
}

void IntegratedViewer::startExecutePythonMission()
{
  if (!onMission)
  {
     if (!isOpened(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    prefixed_layout:=center \
                    my_stack_directory:=" +
           my_stack_directory +" \
                    robot_config_path:=" +
           configuration_folder + ";exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::ENVIRONMENT_VIEWER;
      window_event_pub.publish(msg);
    }

     if (!isOpened(aerostack_msgs::WindowEvent::EXECUTION_VIEWER))
    {
      system(
          ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::EXECUTION_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::EXECUTION_VIEWER;
      window_event_pub.publish(msg);;

    }

    if (!isOpened(aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics Viewer\"  --command \"bash -c 'roslaunch vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                    robot_namespace:=" +
           robot_namespace + ";exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::VEHICLE_DYNAMICS_VIEWER;
      window_event_pub.publish(msg);

    }
    if (!isOpened(aerostack_msgs::WindowEvent::PYTHON_CONTROL))
    {
      namespace pt = boost::property_tree;

      std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                            "graphical_user_interface/layouts/"
                                                                            "layout.json");

      pt::read_json(layout_dir, root);

      std::string height = std::to_string(root.get<int>("PYTHON_MISSION_INTERPRETER.height"));
      std::string width = std::to_string(root.get<int>("PYTHON_MISSION_INTERPRETER.width"));

      std::string x = std::to_string(root.get<int>("PYTHON_MISSION_INTERPRETER.position.x"));
      std::string y = std::to_string(root.get<int>("PYTHON_MISSION_INTERPRETER.position.y"));
      system(
          ("xfce4-terminal  \ --tab --title \"Python Mission Control\"  --command \"bash -c 'roslaunch python_mission_control_window python_mission_control_window.launch --wait \
                    robot_namespace:=" +
           robot_namespace + " \
                    my_stack_directory:=" +
           my_stack_directory +" \
                    mission_configuration_folder:=" +
           my_stack_directory +"/configs/"+robot_namespace+" \; exec bash'\" &")
              .c_str());

      system(
          ("xfce4-terminal  \--title \"Python Based Mission Interpreter\"  --geometry " + width + "x" + height + "+" +
           x + "+" + y +
           "  --command \"bash -c 'roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait\
                    drone_id_namespace:=" +
           robot_namespace + " \
                    mission:=mission.py \
                    my_stack_directory:=" +
           my_stack_directory +" \
                    mission_configuration_folder:=" +
           my_stack_directory +"/configs/"+robot_namespace+" \;exec bash'\" &")
              .c_str());

      openedWindows.push_back(aerostack_msgs::WindowEvent::PYTHON_CONTROL);

      aerostack_msgs::WindowEvent msg;
      msg.window = aerostack_msgs::WindowEvent::PYTHON_CONTROL;
      window_event_pub.publish(msg);
    }
  }
  else
  {
    QMessageBox::warning(nullptr, tr("Warning"),
                       tr("Mission in progress. Land  or abort it first"));
  }
}



void IntegratedViewer::closeEvent(QCloseEvent* event)
{
  aerostack_msgs::WindowEvent msg;
  msg.window = aerostack_msgs::WindowEvent::INTEGRATED_VIEWER;
  msg.event = aerostack_msgs::WindowEvent::CLOSE;
  window_event_pub.publish(msg);
}

void IntegratedViewer::changeEvent(QEvent* evt)
{
  if ((evt->type() == QEvent::WindowStateChange && isMinimized()))
  {
    aerostack_msgs::WindowEvent msg;
    msg.window = aerostack_msgs::WindowEvent::INTEGRATED_VIEWER;
    msg.event = aerostack_msgs::WindowEvent::MINIMIZE;
    window_event_pub.publish(msg);
  }
}
