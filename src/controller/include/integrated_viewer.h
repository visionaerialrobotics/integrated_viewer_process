/*!*********************************************************************************
 *  \file       integrated_viewer.h
 *  \brief      IntegratedViewer definition file.
 *  \authors    Abraham Carrera Groba
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef INTEGRATED_VIEWER_H
#define INTEGRATED_VIEWER_H

#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "aerostack_msgs/WindowEvent.h"
#include <QMainWindow>
#include <QEvent>
#include <QAction>
#include <QIcon>
#include <QMessageBox>
#include <stdlib.h>
#include <thread>
#include <chrono>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"

namespace Ui
{
class IntegratedViewer;
}

class IntegratedViewer : public QMainWindow
{
  Q_OBJECT

public:

  explicit IntegratedViewer(int argc, char** argv, QWidget* parent = 0);

  ros::NodeHandle n;
  ~IntegratedViewer();

private:

  Ui::IntegratedViewer* ui;

  std::string robot_namespace;
  std::string configuration_folder;
  std::string mission_folder;
  std::string my_stack_directory;

  std::string window_event_topic;
  std::string mission_state_topic;

  std::list<aerostack_msgs::WindowEvent::_window_type> openedWindows;

  bool onMission;

  boost::property_tree::ptree root;

  ros::Subscriber mission_state_sub;
  ros::Subscriber window_event_sub;
  ros::Publisher window_event_pub;

  std_msgs::Bool mission_state_msg;
  aerostack_msgs::WindowEvent window_event_msg;

  void connectActions();
  void setUp();
  bool isOpened(aerostack_msgs::WindowEvent::_window_type i);

public Q_SLOTS:

 /* Slots */
  void startGraphicalTeleoperation();
  void startAlphanumericInterface();
  void startExecutePythonMission();
  void startExecuteBehaviorMission();


 /* Callbacks */
  void windowEventCallback(const aerostack_msgs::WindowEvent& msg);
  void missionStateCallback(const std_msgs::Bool& msg);

 /* Events */
  void changeEvent(QEvent* evt);
  void closeEvent(QCloseEvent* event);

};

#endif  // INTEGRATED_VIEWER_H
